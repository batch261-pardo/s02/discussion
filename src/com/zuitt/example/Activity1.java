package com.zuitt.example;

import java.util.Scanner;

public class Activity1 {

    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year.");
        int inputYear = scan.nextInt();

        if (inputYear % 4 == 0 && (inputYear % 100 != 0 || inputYear % 400 == 0)) {
            System.out.println(inputYear + " is a leap year.");
        } else {
            System.out.println(inputYear + " is not a leap year.");
        }
    }
}
