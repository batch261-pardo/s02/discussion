package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    // Java Collection
        // are a single unit of objects
        // useful for manipulating relevant pieces of data that can be used in different situation
    public static void main(String[] args){
            //Array are containers of values of the SAME data type given a pre-defined amount of values.
            // Java arrays are more rigid, once the size and data type are defined, they can no longer be changed

            //Syntax: Array Declaration
                //dataType[] identifier = new dataType[numOfElements]
                //the values of the array is initializes to 0 or null

        int[] intArray = new int[5];
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 98;

        System.out.println(intArray[2]);
        //It prints the memory address of the array
        System.out.println(intArray);
        //to print the intArray
        System.out.println("----------------------------------");
        System.out.println("Int[] " +Arrays.toString(intArray));

        boolean[] boolValue = new boolean[2];
        System.out.println("----------------------------------");
        System.out.println("boolean[] " + Arrays.toString(boolValue));

        String[] stringVal = new String[2];
        System.out.println("----------------------------------");
        System.out.println("String[] " + Arrays.toString(stringVal));

        //Declaring Array with Initialization
        //Syntax:
            //dataType[] identifier = {elementA, elementB, .... elementNth}

        String[] names = {"John", "Jane", "Joe"};
        System.out.println("----------------------------------");
        System.out.println(Arrays.toString(names) + " Array Length is " + names.length);

        //Sample Java Array methods
        String searchTerm = "John";
        int binaryResult = Arrays.binarySearch(names, searchTerm);
        System.out.println(binaryResult);

        //Multi-dimensional Arrays
        //Syntax:
            //dataType[][] identifier  = new dataType[rowLength][colLength]

        String[][] classroom = new String[3][3];

        //First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        //First Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Jane";
        classroom[1][2] = "Jobert";

        //First Row
        classroom[2][0] = "Mickel";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        System.out.println(Arrays.toString(classroom));
        //we use the deepToString() method for printing multidimensional array
        System.out.println(Arrays.deepToString(classroom));

        //ArrayLists
            //are resizable arrays, wherein elements can be added or removed whenever it is needed.

            //Syntax:
                //ArrayList<dataType> identifier = new ArrayList<dataType>()

            //Declare an arrayList
            ArrayList<String> students = new ArrayList<>();

            //Add elements to ArrayList is done by the add() function
            students.add("John");
            students.add("Paul");

            System.out.println(students);

            //Declare an ArrayList with values
        ArrayList<String> students1 = new ArrayList<>(Arrays.asList("Banner", "Tony"));
        System.out.println(students1);

        //Accessing elements to an ArrayList is  done by the get() function
        System.out.println(students1.get(1));

        //Updating an element
        //arrayListName.set(index, element)
        students1.set(1, "T'Challa");
        System.out.println(students1);

        //Adding an element on a specific index
        //arrayListName.add(index, value/element)
        students1.add(1, "Bakky");
        System.out.println(students1);

        //Removing a specific element
        students1.remove(1);
        System.out.println(students1);

        //Remove all elements
        students1.clear();
        System.out.println(students1);

        //Getting the number of elements in an ArrayList
        System.out.println(students1.size());

        ArrayList<Integer> num = new ArrayList<>(Arrays.asList(12, 22));
        System.out.println(num);

        //HashMaps
            //collection of data in "key-value pairs"
            //in Java, "keys" also referred by the "fields"
            //wherein the values are accessed by the "fields"
        //Syntax:
            //HashMap<dataTypeField, dataTypeValue> identifier = new HashMaps<dataTypeField, dataTypeValue>();

        //Declaring HashMaps
        HashMap<String, String> jobPosition = new HashMap<>();
        System.out.println(jobPosition);

        //Add elements to HashMap by using the put() function
        jobPosition.put("Student", "Alice");
        jobPosition.put("Web Developer", "Ian");
        System.out.println(jobPosition);

        //Creating HashMap with Initialization
        HashMap<String, String> jobPos = new HashMap<>(){
            {
                put("Cyber Security", "Alice");
                put("Database Admin", "Steve");
            }
        };
        System.out.println(jobPos);

        //Accessing elements in HashMaps
        System.out.println(jobPos.get("Database Admin"));

        //updating a HashMaps
        jobPos.replace("Database Admin", "Killua");
        System.out.println(jobPos);

        //for printing/getting keys in HashMaps
        System.out.println(jobPos.keySet());
        //for printing/getting values in HashMaps
        System.out.println(jobPos.values());

        //not updating but adding a new value to existing key
        jobPos.put("Cyber Security", "Johnson");
        System.out.println(jobPos);

        //Removing a field name
        jobPos.remove("Database Admin");
        System.out.println(jobPos);

        //Removing all elements
        jobPos.clear();
        System.out.println(jobPos);

    }
}
