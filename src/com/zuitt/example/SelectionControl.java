package com.zuitt.example;

import java.util.Scanner;

public class SelectionControl {
    public static void main(String[] arg){
        //Java Operators
            //Arithmetic -> +, -, *, /, %
            //Comparison -> >, <, >=, <=, ==, !=
            //Logical -> &&, ||, !
            //Assignment -> =

        // Selection Control Structure in Java
        // if else
        //Syntax:
            /*
            if(condition){
                //code block
            }else{
                //code block
            }
            */
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int num = scan.nextInt();
        if(num % 5 == 0){
            System.out.println("divisible by 5");
        }else{
            System.out.println("not divisible by 5");
        }

        //Short Circuiting
        int x = 15;
        int y = 0;

        if(y != 0 && x/y == 0){
            System.out.println("Result is: " + x/y);
        }else{
            System.out.println("This will only run because of short circuiting");
        }

        //Ternary Operator
        int number = 25;
        Boolean result = (number > 0) ? true : false;
        System.out.println(result);

        //Switch Cases
        System.out.println("Enter a number from 1-4, to print four direction.");
        int directionValue = scan.nextInt();

        switch(directionValue){
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid");
                break;
        }
    }
}
