package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class Activity2 {
    public static void main(String[] args){
        //Array Integers Prime Number
        int[] intArray = new int[5];
        intArray[0] = 2;
        intArray[1] = 3;
        intArray[2] = 5;
        intArray[3] = 7;
        intArray[4] = 11;
        System.out.println("-----------------------------------------");
        System.out.println("The first prime number is: " + intArray[0]);

        //Array Lists
        ArrayList<String> customers = new ArrayList<>();
        customers.add("John");
        customers.add("Jane");
        customers.add("Chloe");
        customers.add("Zoey");
        System.out.println("-----------------------------------------");
        System.out.println("My friends are: " + customers);

        //HashMap
        HashMap<String, Integer> products = new HashMap<>(){
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("-----------------------------------------");
        System.out.println("Our current inventory consists of: " + products);
    }
}
